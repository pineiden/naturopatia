from django.contrib import admin
from .models import Clasificacion, Etiqueta, Articulo
# Register your models here.

class ArticuloAdmin(admin.ModelAdmin):
	list_display = ("clasificacion","titulo","fecha_publicacion")
	class Media:
		js = ('ckeditor/ckeditor/ckeditor.js')


admin.site.register(Articulo,ArticuloAdmin)
admin.site.register(Clasificacion)
admin.site.register(Etiqueta)
