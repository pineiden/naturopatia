from django.shortcuts import render
from .models import Clasificacion, Etiqueta, Articulo
from django.conf import settings
from django.shortcuts import get_object_or_404, render
from django.views.generic import ListView, DetailView

# Create your views here.

class ArticuloView(DetailView):
    template_name = "articulo/articulo.html"
    model = Articulo

class ArticulosView(ListView):
    template_name = "articulo/articulos.html"
    model = Articulo

class ClasificacionView(DetailView):
    template_name = "articulo/clasificacion.html"
    model = Clasificacion

class ClasificacionesView(ListView):
    template_name = "articulo/clasificacion.html"
    model = Clasificacion

class EtiquetaView(DetailView):
    template_name = "articulo/etiqueta.html"
    model = Etiqueta

class EtiquetassView(ListView):
    template_name = "articulo/etiquetas.html"
    model = Etiqueta

