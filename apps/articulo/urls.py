from django.urls import path
from .views import ArticulosView, ArticuloView

app_name="articulo"

urlpatterns = [
    path('articulos',
         ArticulosView.as_view(),
         name='articulos'),
    path('articulo/<slug:slug_titulo>',
         ArticuloView.as_view(),
         name='articulo_slug'),
    path('articulo/<int:pk>',
         ArticulosView.as_view(),
         name='articulo_id'),
]
