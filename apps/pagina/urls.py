from django.urls import path, include
from django.conf import settings
from . import views


app_name="pagina"

urlpatterns = [
    path('<slug:slug_titulo>',
         views.ver_pagina,
         name='ver_pagina'),
    path('clasificacion/<slug:slug_tipo>',
         views.ver_clasificacion),
    path('listar',
         views.PaginaList.as_view(),
         name='pagina_list'),
    path('agregar',
         views.PaginaCreate.as_view(),
         name='pagina_new'),
    path('editar/(<int:pk>',
         views.PaginaUpdate.as_view(),
         name='pagina_edit'),
    path('borrar/<int:pk>',
         views.PaginaDelete.as_view(),
         name='pagina_delete'),
    path('clasificacion-autocomplete/',
         views.ClasificacionAutocomplete.as_view( create_field = 'tipo' ),
         name='clasificacion-autocomplete')
]
