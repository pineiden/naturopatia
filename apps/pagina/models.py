# Create your models here.
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from . import managers
#ckeditor model
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
#slug
from autoslug import AutoSlugField
import itertools
from django.utils import timezone
from datetime import datetime,time
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


from django.urls import reverse

def get_upload_pagina_file_name(instance, filename):
    return "paginas/%s/%s" %(instance.slug_titulo, filename)

class Clasificacion(models.Model):
    tipo = models.CharField(max_length=20, unique=True)
    slug_tipo = AutoSlugField(
        blank=True,
        populate_from='tipo',
        editable=True)
    descripccion = models.TextField(blank=True)
    fecha_publicacion = models.DateTimeField(auto_now=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def get_absolute_url(self):
        return reverse('clasificacion:post',
                       kwargs={'slug_tipo':self.slug_tipo})

    @property
    def Clasificacion(self):
        return self.tipo

    class Meta:
        app_label = "pagina"
        verbose_name = _("Clasificación")
        verbose_name_plural = _("Clasificaciones")
        ordering = ("tipo","fecha_publicacion")

    def __str__(self):
        return self.tipo

class Pagina(models.Model):
    titulo = models.CharField(max_length=100)
    slug_titulo =  AutoSlugField(unique=True,blank=True,populate_from='titulo', editable=True)
    imagen = models.ImageField(
        upload_to=get_upload_pagina_file_name,
        blank=True)
    fecha_publicacion = models.DateTimeField(auto_now=True)
    resumen = models.TextField(default="Escribe un resumen")
    cuerpo = RichTextUploadingField(help_text="Escribe todo el contenido")
    clasificacion = models.ForeignKey(Clasificacion,on_delete=models.CASCADE,blank=True,null=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True, editable=False)
    object_id = models.PositiveIntegerField(null=True, blank=True, editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    def get_absolute_url(self):
        return reverse('pagina_edit', kwargs={'pk':self.pk})

    @property
    def Pagina(self):
        return self.titulo

    class Meta:
        app_label = "pagina"
        verbose_name = _("Página")
        verbose_name_plural = _("Páginas")
        ordering = ("clasificacion","fecha_publicacion")

    def __str__(self):
        return self.titulo

