from django.urls import path, include
from django.conf import settings
from .views import PatologiasView


app_name="patologia"

urlpatterns = [
    path('patologias',
         PatologiasView.as_view(),
         name='patologias'),
]
