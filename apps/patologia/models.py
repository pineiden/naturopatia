from django.db import models
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse


def get_upload_pagina_file_name(instance, filename):
    return "patologias/%s/%s" %(instance.slug_nombre, filename)

class Patologia(models.Model):
    nombre = models.CharField(
        max_length=50)
    imagen = models.ImageField(
        upload_to=get_upload_pagina_file_name)
    slug_nombre = AutoSlugField(
        blank=True,
        populate_from='nombre',
        editable=True)
    descripcion = RichTextField(
        verbose_name='Descripción')
    sintomas = RichTextField(
        verbose_name='Síntomas')
    causas = RichTextField(
        verbose_name='Causas')
    fecha_publicacion = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('patologia',
                       kwargs={
                           'pk':self.pk,
                           'slug':self.slug_nombre
                       })

    @property
    def Patologia(self):
        return self.nombre

    class Meta:
        app_label = "patologia"
        verbose_name = _("Patología")
        verbose_name_plural = _("Patologías")
        ordering = ("nombre","fecha_publicacion")

    def __str__(self):
        return self.nombre

