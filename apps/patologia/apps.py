from django.apps import AppConfig


class PatologiaConfig(AppConfig):
    name = 'patologia'
