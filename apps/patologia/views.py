from django.shortcuts import render
from .models import Patologia
# Create your views here.
from django.views.generic.list import ListView

class PatologiasView(ListView):
    template_name = "patologia/patologias.html"
    model = Patologia
