from django.shortcuts import render
from .models import Especialista
from django.views.generic.list import ListView

class EspecialistasView(ListView):
    template_name = 'especialista/especialistas.html'
    model = Especialista
