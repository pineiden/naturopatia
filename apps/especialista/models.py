from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField

def get_upload_pagina_file_name(instance, filename):
    return "especialistas/%s/%s" %(instance.slug_nombre, filename)

class Especialista(models.Model):
    CARGOS = (('PR','Presidente'),
              ('ASH','Naturopata Asociado'),
              ('ASM', 'Naturopata Asociada'))
    nombre = models.CharField(
        max_length=50,
        blank=False)
    slug_nombre =  AutoSlugField(
        blank=True,
        populate_from='nombre',
        editable=True)
    cargo = models.CharField(
        max_length=5,
        blank=False,
        choices=CARGOS,
        default='ASM')
    email = models.EmailField()
    carnet = models.ImageField(
        upload_to=get_upload_pagina_file_name,
        blank=True)
    rut = models.CharField(
        max_length=20)
    telefono = PhoneNumberField(
        verbose_name='Teléfono')
    orden = models.IntegerField(blank=True, default=0)
    linkedin = models.URLField(
        verbose_name='Perfil Linkedin',
        blank=True)
    twitter =  models.URLField(
        verbose_name='Perfil Twitter',
        blank=True)


    def get_absolute_url(self):
        return reverse('especialista_edit', kwargs={'pk':self.pk})

    @property
    def Especialista(self):
        return self.nombre

    class Meta:
        app_label = "especialista"
        verbose_name = _("Especialista")
        verbose_name_plural = _("Especialistas")
        ordering = ("orden","nombre")

    def __str__(self):
        return self.nombre

    def get_cargo(self):
        cargos=dict(self.CARGOS)
        return cargos.get(self.cargo)



