from django.urls import path
from .views import EspecialistasView

app_name="especialista"

urlpatterns = [
    path('especialistas',
         EspecialistasView.as_view(),
         name='especialistas'),
]
