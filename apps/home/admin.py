from django.contrib import admin

# Register your models here.
from .models import ConfiguracionHome, SetHomeImage, HomeImage

class ConfiguracionHomeAdmin(admin.ModelAdmin):
    pass
admin.site.register(ConfiguracionHome, ConfiguracionHomeAdmin)


class HomeImageAdmin(admin.ModelAdmin):
    pass
admin.site.register(HomeImage, HomeImageAdmin)


class SetHomeImageAdmin(admin.ModelAdmin):
    pass
admin.site.register(SetHomeImage, SetHomeImageAdmin)
