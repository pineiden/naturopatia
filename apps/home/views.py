from django.shortcuts import render
from django.core import serializers
# Create your views here.

# some_app/views.py
from django.views.generic import TemplateView

# from models

from .models import (SetHomeImage,
                     HomeImage,
                     ConfiguracionHome)

class HomeView(TemplateView):
    template_name = "home/home.html"

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView,
                        self).get_context_data(*args,
                                               **kwargs)
        configuracion = ConfiguracionHome.objects.filter(activar_conf=True).first()
        # Get id SetHomeImage
        if configuracion:
            set_imagenes = getattr(configuracion,'set_images')
            images = HomeImage.objects.filter(set_images=set_imagenes)
            more_data = {
                'configuracion': configuracion,
                'imagenes': images
            }
            context.update(more_data)
        return context
