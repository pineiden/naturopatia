from django.db import models
import simplejson as json
from django.utils import timezone
from django.urls import reverse

# Create your models here.

class SetHomeImage(models.Model):
    """
    Define un grupo de imágenes a mostrar en Home
    """
    nombre = models.CharField(max_length=200)
    fecha = models.DateField(default=timezone.now)

    def get_absolute_url(self):
        return reverse('setimageshome_detail', kwargs={'pk':self.pk})

    def __str__(self):
        return self.nombre

    class Meta:
        app_label='home'
        verbose_name='set-imagenes'
        verbose_name_plural='sets-imagenes'

class HomeImage(models.Model):
    """
    Define imagenes y las asigna a un grupo
    """
    imagen = models.ImageField(upload_to='home/imagen')
    etiqueta = models.CharField(max_length=100)
    set_images = models.ForeignKey(SetHomeImage,
                                   related_name='home_images',
                                   blank=True,
                                   null=True,
                                   on_delete=models.CASCADE)

    def __str__(self):
        return self.etiqueta

    class Meta:
        app_label='home'
        verbose_name='imagen'
        verbose_name_plural='imágenes'

from autoslug import AutoSlugField

class ConfiguracionHome(models.Model):
    nombre = models.CharField(max_length=200,
                              help_text="Nombre de la configuración en particular")
    slug_nombre = AutoSlugField(blank=True,
                                populate_from='nombre',
                                editable=False)
    set_images =  models.ForeignKey(SetHomeImage,
                                    related_name='configuraciones_home',
                                    blank=True,null=True,
                                    on_delete=models.CASCADE)#--->borrar
    mostrar_especialista = models.BooleanField(
        default=True,
        help_text="Mostrar especialista aleatorio")
    mostrar_especialista = models.BooleanField(
        default=True,
        help_text="Mostrar ")
    activar_blog = models.BooleanField(
        default=False,
        blank = True,
        help_text="¿Deseas activar el blog?")
    activar_conf = models.BooleanField(
        default=False,
        blank=False,
        help_text="Activa esta configuración")

    def save(self, control=True, *args, **kwargs):
        """
        Este método desactiva las configuraciones del resto de las configuraciones
        de manera que sea una única configuración activa
        """
        if not control:
            query_actives = ConfiguracionHome.objects.filter(activar_conf=True)
            def off_conf(instance):
                instance.activar_conf = False
                instance.save(control=False, *args, **kwargs)
            list(map(off_conf,query_actives))
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('configuracion:post', args=(self.slug_nombre,))

    def __str__(self):
        return self.nombre

    class Meta:
        app_label='home'
        verbose_name='configuracion'
        verbose_name_plural='configuraciones'


