from django.contrib import admin

# Register your models here.
from .models import Especialidad

class EspecialidadAdmin(admin.ModelAdmin):
    pass
admin.site.register(Especialidad, EspecialidadAdmin)

