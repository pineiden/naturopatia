from django.db import models
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.utils.translation import ugettext_lazy as _

# Create your models here.

def get_upload_pagina_file_name(instance, filename):
    return "especialidades/%s/%s" %(instance.slug_titulo, filename)

class Especialidad(models.Model):
    titulo = models.CharField(max_length=100)
    slug_titulo =  AutoSlugField(unique=True,blank=True,populate_from='titulo', editable=True)
    imagen = models.ImageField(upload_to=get_upload_pagina_file_name, blank=True)
    fecha_publicacion = models.DateTimeField(auto_now=True)
    resumen = models.TextField(default="Escribe un resumen")
    cuerpo = RichTextUploadingField(help_text="Escribe todo el contenido")

    def get_absolute_url(self):
        return reverse('especialidad_edit', kwargs={'pk':self.pk})

    @property
    def Especialidad(self):
        return self.titulo

    class Meta:
        app_label = "especialidad"
        verbose_name = _("Especialidad")
        verbose_name_plural = _("Especialidades")
        ordering = ("fecha_publicacion","titulo")

    def __str__(self):
        return self.titulo


