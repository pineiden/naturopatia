from django.shortcuts import render
from .models import Especialidad
# Create your views here.
from django.views.generic.list import ListView

class EspecialidadesView(ListView):
    template_name = "especialidad/especialidades.html"
    model = Especialidad

