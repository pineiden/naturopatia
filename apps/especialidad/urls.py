from django.urls import path
from .views import EspecialidadesView

app_name="especialidad"

urlpatterns = [
    path('especialidades', EspecialidadesView.as_view(), name='especialidad'),
]
