# Configuracion CKEDITOR

CKEDITOR_UPLOAD_SLUGIFY_FILENAME = True
CKEDITOR_RESTRICT_BY_USER = True
CKEDITOR_BROWSE_SHOW_DIRS = True

# Upload path
CKEDITOR_UPLOAD_PATH = "uploads/"

CKEDITOR_IMAGE_BACKEND = 'pillow'

CKEDITOR_CONFIGS = {
    'default': {
        'removePlugins': 'stylesheetparser', 'bbcode'
        'toolbar': None,
        'height': 500,
        'width': 900,
        'tabSpaces': 4,
        'allowedContent': True,
        'extraPlugins': ','.join(
            [
                # you extra plugins here
                'ajax',
                'div',
                'autolink',
                'autoembed',
                'embedsemantic',
                'autogrow',
                # 'devtools',code,
                'image',
                'widget',
                'lineutils',
                'clipboard',
                'dialog',
                'dialogui',
                'codesnippetgeshi',
                'elementspath',
                'table',
                'tableresize',
                'tabletools',
                'templates',
                'uicolor',
                'uploadimage',
                'uploadwidget',
                'mathjax',
                'pagebreak',
                'placeholder'
            ]),
    },
}


# Bleach filter html
BLEACH_DEFAULT_WIDGET = 'ckeditor.widgets.CKEditorWidget'
# Which HTML tags are allowed
BLEACH_VALID_TAGS = ['p', 'b', 'i', 'strike', 'ul', 'li', 'ol', 'br',
                     'span', 'blockquote', 'hr', 'a', 'img']
BLEACH_VALID_ATTRS = {
    'span': ['style', ],
    'p': ['align', ],
    'a': ['href', 'rel'],
    'img': ['src', 'alt', 'style'],
}
BLEACH_VALID_STYLES = ['color', 'cursor', 'float', 'margin']
BLEACH_ALLOWED_ATTRIBUTES = ['href', 'title', 'name']
BLEACH_STRIP_TAGS = True

# DATE FORMAT

DATE_INPUT_FORMATS = ['%d/%m/%Y', '%d %m %Y', '%d-%m-%Y']

