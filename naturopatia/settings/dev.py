import os
from .base import *
DEBUG = True

# Add livereload to INSTALLED_APPS

buscar = 'django.contrib.admin'
index = INSTALLED_APPS.index(buscar)
INSTALLED_APPS.insert(index-1, 'livereload')
MIDDLEWARE.append('livereload.middleware.LiveReloadScript')
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

"""
DBNAME=get_env_variable('DBNAME')
DBUSER=get_env_variable('DBUSER')
DBPASS=get_env_variable('DBPASS')

DATABASES = {
    'default': {
         'ENGINE': 'django.contrib.gis.db.backends.postgis',
         'NAME': DBNAME,
         'USER': DBUSER,
         'PASSWORD':DBPASS,
         'HOST': '',
         'PORT': '',
    },
}
"""

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

SITE_URL="http://www.naturopatiachile.cl"
SITE_NAME="Naturopatia Chile"
GOOGLE_API_KEY=''
SITE_ID = 1
ALLOWED_HOSTS=["*"]
