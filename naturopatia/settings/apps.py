# Application definition

INSTALLED_APPS = [
    'dal',
    'dal_select2',
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    #'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.sites',
    # admin docs
    "django.contrib.admindocs",
    # CKEDITOR
    'ckeditor',
    # uploader
    'ckeditor_uploader',
   # Django bleach: HTML sanitized
    'django_bleach',
    'phonenumber_field',
    'apps.home',
    'apps.pagina',
    'apps.especialidad',
    'apps.especialista',
    'apps.patologia',
    'apps.articulo',
]
