import os
from .base import *

DEBUG_DATA = get_env_variable('DEBUG')
DEBUG = False
if DEBUG_DATA == 'TRUE':
    DEBUG = True

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DBNAME=get_env_variable('DATABASE_NAME')
DBUSER=get_env_variable('DATABASE_USER')
DBPASS=get_env_variable('DATABASE_PASS')

engine='django.db.backends.postgresql_psycopg2'

DATABASES = {
    'default': {
         'ENGINE': engine,
         'NAME': DBNAME,
         'USER': DBUSER,
         'PASSWORD':DBPASS,
         'HOST': '',
         'PORT': '',
    },
}


SITE_URL="http://www.naturopatiachile.cl"
SITE_NAME="Naturopatia Chile"
ALLOWED_HOSTS=['localhost',
               '127.0.0.1',
               '[::1]',
               '178.128.181.47',
               'www.naturopatiachile.cl',
               'naturopatiachile.cl',]
SITE_ID = 1
GOOGLE_API_KEY=''
