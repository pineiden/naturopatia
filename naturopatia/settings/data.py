CONTACTO = {
    'persona': "Juan Panay",
    'direccion': "Manuela Errázuriz #4510",
    'comuna': "Pedro Aguirre Cerda",
    'telefono':"56982710865",
    'facebook':None,
    'twitter':None,
    'instagram':None,
    'linkedin':None
}

SITIO = {
    'nombre':'Asociación de Apiterapeutas y Naturópatas de Chile',
    'frase':'¡Por la Salud de los Pueblos!',
    'web':'Naturopatía Chile'
}

import json

class TextoLink:
    def __init__(self, texto, link='/'):
        self.texto=texto
        self.link=link
    def __repr__(self):
        return json.dumps(self.__dict__)

NAV_MENU = [
    ('home', TextoLink('Inicio')),
    ('noticias',TextoLink('Noticias', link="/articulos")),
    ('nosotros', TextoLink('Quienes Somos',link='/pagina/quienes-somos')),
    ('especialidades', TextoLink('Especialidades', link='/especialidades')),
    ('especialistas', TextoLink('Especialistas', link='/especialistas')),
    ('patologias', TextoLink('Patologías', link='/patologias')),
    ]


NV = 'seven'
