"""
WSGI config for naturopatia project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
from micro_tools.environment.get_env import get_env_variable

from django.core.wsgi import get_wsgi_application

DJANGO_STATUS=get_env_variable("DJANGO_STATUS")
os.environ.setdefault("DJANGO_SETTINGS_MODULE",DJANGO_STATUS)

application = get_wsgi_application()
