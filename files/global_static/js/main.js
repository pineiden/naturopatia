// function to set the height on fly
function autoHeight() {
    $('#all_content').css('min-height', 0);
    $('#all_content').css('min-height', (
        $(document).height() 
            - $('#nav_menu').height() 
            - $('#footer').height()
    ));
}

// onDocumentReady function bind
$(document).ready(function() {
    autoHeight();
});

// onResize bind of the function
$(window).resize(function() {
    autoHeight();
});
