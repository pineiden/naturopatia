import os, sys, django
BASE_DIR = os.path.dirname(os.path.dirname(
	os.path.dirname(
		os.path.dirname(
			os.path.abspath(__file__)
			)
		)
	))

sys.path.append(BASE_DIR)
sys.path.append(BASE_DIR +'/naturopatia')
from micro_tools.environment.get_env import get_env_variable
DJANGO_STATUS=get_env_variable("DJANGO_STATUS")
os.environ.setdefault("DJANGO_SETTINGS_MODULE",DJANGO_STATUS)

import django
django.setup()
from apps.pagina.models import Clasificacion, Pagina
import csv, codecs


header=[
    "langcode",
    "type",
    "status",
    "title",
    "uid",
    "created",
    "changed",
    "default_langcode",
    "moderation_state",
    "path",
    "body"
]

utiles = ['title', 'body']

keys_pag = dict(zip(utiles,['titulo','cuerpo',] ))

def load_paginas():
    info = 'Informativa'
    query_tipo = Clasificacion.objects.filter(tipo=info).first()
    tipo = query_tipo
    if not query_tipo:
        tipo = Clasificacion(tipo=info)
        tipo.save()
    fpath = os.path.dirname(os.path.abspath(__file__))
    print(fpath)
    filename = fpath+'/pagina.csv'
    with open(filename, 'r') as archivo:
        reader = csv.DictReader(archivo, fieldnames=header)
        count = 0
        for row in reader:
            if count >0:
                new_pag = {'clasificacion':tipo}
                [print(row.get(field)) for field in utiles]
                [new_pag.update(
                    {
                        keys_pag.get(field):row.get(field)
                    }
                ) for field in utiles]
                pag = Pagina(**new_pag)
                pag.save()
            count += 1

def __main__():
	load_paginas()

if __name__ == "__main__":
    __main__()
