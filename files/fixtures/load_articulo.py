import os, sys, django
BASE_DIR = os.path.dirname(os.path.dirname(
	os.path.dirname(
		os.path.dirname(
			os.path.abspath(__file__)
			)
		)
	))

sys.path.append(BASE_DIR)
sys.path.append(BASE_DIR +'/naturopatia')

from micro_tools.environment.get_env import get_env_variable
DJANGO_STATUS=get_env_variable("DJANGO_STATUS")
os.environ.setdefault("DJANGO_SETTINGS_MODULE",DJANGO_STATUS)

import django
django.setup()
from apps.especialista.models import Especialista
import csv, codecs

header=[
    "nid",
    "langcode",
    "type",
    "status",
    "title",
    "uid",
    "created",
    "changed",
    "default_langcode",
    "moderation_state",
    "path",
    "body",
    "field_image",
    "field_tags",
    "field_tipo_de_articulo"
]

utiles = ['title',
          'body',
          'field_tags',
          'field_tipo_de_articulo']

keys_pag = dict(zip(utiles,['nombre','cargo', 'email', 'rut', 'telefono'] ))

cargos = {
    "Presidente Asociación chilena de Naturópatas": "PR",
    "Natúropata Asociado": "ASH",
    "Natúropata Asociada": "ASM",
}

def load_especialista():
    fpath = os.path.dirname(os.path.abspath(__file__))
    print(fpath)
    filename = fpath+'/articulos.csv'
    with open(filename, 'r') as archivo:
        reader = csv.DictReader(archivo, fieldnames=header)
        for row in reader:
            new_esp = {}
            [print(row.get(field)) for field in utiles]
            for field in utiles:
                if field=='field_cargo':
                    new_esp.update({
                        keys_pag.get(field):cargos.get(row.get(field),"ASM")})
                else:
                    new_esp.update({
                        keys_pag.get(field):row.get(field)
                    })
            esp = Especialista(**new_esp)
            esp.save()

def __main__():
	load_especialista()

if __name__ == "__main__":
    __main__()
