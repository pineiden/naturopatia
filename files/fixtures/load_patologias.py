import os, sys, django
BASE_DIR = os.path.dirname(os.path.dirname(
	os.path.dirname(
		os.path.dirname(
			os.path.abspath(__file__)
			)
		)
	))

sys.path.append(BASE_DIR)
sys.path.append(BASE_DIR +'/naturopatia')
from micro_tools.environment.get_env import get_env_variable
DJANGO_STATUS=get_env_variable("DJANGO_STATUS")
os.environ.setdefault("DJANGO_SETTINGS_MODULE",DJANGO_STATUS)

import django
django.setup()
from apps.patologia.models import Patologia
import csv, codecs

header=[
    "langcode",
    "type",
    "status",
    "title",
    "uid",
    "created",
    "changed",
    "default_langcode",
    "moderation_state",
    "path",
    "body",
    "field_causas",
    "field_imagen",
    "field_sintomas"
]

utiles = ['title', 'body', 'field_causas', 'field_sintomas']

keys_pat = dict(zip(utiles,[
    'nombre',
    'descripcion',
    'causas',
    'sintomas',] ))

def load_patologia():
    fpath = os.path.dirname(os.path.abspath(__file__))
    print(fpath)
    filename = fpath+'/patologia.csv'
    with open(filename, 'r') as archivo:
        reader = csv.DictReader(archivo, fieldnames=header)
        count = 0
        for row in reader:
            if count>0:
                new_pat = {}
                [print(row.get(field)) for field in utiles]
                [new_pat.update(
                    {
                        keys_pat.get(field):row.get(field)
                    }
                ) for field in utiles]
                pag = Patologia(**new_pat)
                pag.save()
            count += 1

def __main__():
	load_patologia()

if __name__ == "__main__":
    __main__()
