PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "django_migrations" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "app" varchar(255) NOT NULL, "name" varchar(255) NOT NULL, "applied" datetime NOT NULL);
INSERT INTO "django_migrations" VALUES(1,'contenttypes','0001_initial','2018-09-06 02:10:31.506948');
INSERT INTO "django_migrations" VALUES(2,'auth','0001_initial','2018-09-06 02:10:31.674967');
INSERT INTO "django_migrations" VALUES(3,'admin','0001_initial','2018-09-06 02:10:31.832136');
INSERT INTO "django_migrations" VALUES(4,'admin','0002_logentry_remove_auto_add','2018-09-06 02:10:32.000414');
INSERT INTO "django_migrations" VALUES(5,'admin','0003_logentry_add_action_flag_choices','2018-09-06 02:10:32.402755');
INSERT INTO "django_migrations" VALUES(6,'contenttypes','0002_remove_content_type_name','2018-09-06 02:10:32.558333');
INSERT INTO "django_migrations" VALUES(7,'articulo','0001_initial','2018-09-06 02:10:32.806556');
INSERT INTO "django_migrations" VALUES(8,'auth','0002_alter_permission_name_max_length','2018-09-06 02:10:32.963546');
INSERT INTO "django_migrations" VALUES(9,'auth','0003_alter_user_email_max_length','2018-09-06 02:10:33.120579');
INSERT INTO "django_migrations" VALUES(10,'auth','0004_alter_user_username_opts','2018-09-06 02:10:33.248399');
INSERT INTO "django_migrations" VALUES(11,'auth','0005_alter_user_last_login_null','2018-09-06 02:10:33.392887');
INSERT INTO "django_migrations" VALUES(12,'auth','0006_require_contenttypes_0002','2018-09-06 02:10:33.450042');
INSERT INTO "django_migrations" VALUES(13,'auth','0007_alter_validators_add_error_messages','2018-09-06 02:10:33.570659');
INSERT INTO "django_migrations" VALUES(14,'auth','0008_alter_user_username_max_length','2018-09-06 02:10:33.703974');
INSERT INTO "django_migrations" VALUES(15,'auth','0009_alter_user_last_name_max_length','2018-09-06 02:10:33.848452');
INSERT INTO "django_migrations" VALUES(16,'dashboard','0001_initial','2018-09-06 02:10:34.096511');
INSERT INTO "django_migrations" VALUES(17,'especialidad','0001_initial','2018-09-06 02:10:34.242414');
INSERT INTO "django_migrations" VALUES(18,'especialista','0001_initial','2018-09-06 02:10:34.388506');
INSERT INTO "django_migrations" VALUES(19,'especialista','0002_auto_20180902_1045','2018-09-06 02:10:34.556459');
INSERT INTO "django_migrations" VALUES(20,'home','0001_initial','2018-09-06 02:10:34.780269');
INSERT INTO "django_migrations" VALUES(21,'menu','0001_initial','2018-09-06 02:10:34.937246');
INSERT INTO "django_migrations" VALUES(22,'pagina','0001_initial','2018-09-06 02:10:35.127560');
INSERT INTO "django_migrations" VALUES(23,'patologia','0001_initial','2018-09-06 02:10:35.286320');
INSERT INTO "django_migrations" VALUES(24,'sessions','0001_initial','2018-09-06 02:10:35.454502');
INSERT INTO "django_migrations" VALUES(25,'sites','0001_initial','2018-09-06 02:10:35.678242');
INSERT INTO "django_migrations" VALUES(26,'sites','0002_alter_domain_unique','2018-09-06 02:10:35.835342');
INSERT INTO "django_migrations" VALUES(27,'articulo','0002_auto_20180905_2318','2018-09-06 02:18:33.409537');
CREATE TABLE IF NOT EXISTS "auth_group" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(80) NOT NULL UNIQUE);
CREATE TABLE IF NOT EXISTS "auth_group_permissions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "group_id" integer NOT NULL REFERENCES "auth_group" ("id") DEFERRABLE INITIALLY DEFERRED, "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE IF NOT EXISTS "auth_user_groups" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED, "group_id" integer NOT NULL REFERENCES "auth_group" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE IF NOT EXISTS "auth_user_user_permissions" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED, "permission_id" integer NOT NULL REFERENCES "auth_permission" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE IF NOT EXISTS "django_admin_log" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "action_time" datetime NOT NULL, "object_id" text NULL, "object_repr" varchar(200) NOT NULL, "change_message" text NOT NULL, "content_type_id" integer NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED, "action_flag" smallint unsigned NOT NULL);
INSERT INTO "django_admin_log" VALUES(1,'2018-09-06 02:11:56.096179','1','Operativo','[{"added": {}}]',20,1,1);
INSERT INTO "django_admin_log" VALUES(2,'2018-09-06 02:12:02.362393','1','La Pincoya','[{"added": {}}]',19,1,1);
INSERT INTO "django_admin_log" VALUES(3,'2018-09-06 02:19:21.050092','1','Primer operativo en la Pincoya','[{"added": {}}]',18,1,1);
INSERT INTO "django_admin_log" VALUES(4,'2018-09-06 02:22:05.405541','1','Base','[{"added": {}}]',11,1,1);
INSERT INTO "django_admin_log" VALUES(5,'2018-09-06 02:22:11.231020','1','Base','[{"added": {}}]',10,1,1);
INSERT INTO "django_admin_log" VALUES(6,'2018-09-06 02:22:39.509409','1','grupo','[{"added": {}}]',12,1,1);
INSERT INTO "django_admin_log" VALUES(7,'2018-09-06 02:24:52.762266','1','title','',15,1,3);
INSERT INTO "django_admin_log" VALUES(8,'2018-09-06 02:25:08.111917','3','title','',16,1,3);
INSERT INTO "django_admin_log" VALUES(9,'2018-09-06 02:25:08.248983','1','title','',16,1,3);
INSERT INTO "django_admin_log" VALUES(10,'2018-09-06 02:25:19.038165','2','Primer operativo en la Pincoya','',16,1,3);
INSERT INTO "django_admin_log" VALUES(11,'2018-09-06 02:25:44.750601','1','title','',17,1,3);
INSERT INTO "django_admin_log" VALUES(12,'2018-09-06 02:26:17.159688','6','Charles Varga','[{"changed": {"fields": ["nombre", "carnet"]}}]',16,1,2);
INSERT INTO "django_admin_log" VALUES(13,'2018-09-06 02:26:30.799237','5','Daniel Fuentes','[{"changed": {"fields": ["nombre", "carnet"]}}]',16,1,2);
INSERT INTO "django_admin_log" VALUES(14,'2018-09-06 02:26:41.344260','9','Gladys Muñoz','[{"changed": {"fields": ["nombre", "carnet"]}}]',16,1,2);
INSERT INTO "django_admin_log" VALUES(15,'2018-09-06 02:26:50.666413','4','Juan Panay','[{"changed": {"fields": ["nombre", "carnet"]}}]',16,1,2);
INSERT INTO "django_admin_log" VALUES(16,'2018-09-06 02:27:01.499913','10','María Fuentes','[{"changed": {"fields": ["nombre", "carnet"]}}]',16,1,2);
INSERT INTO "django_admin_log" VALUES(17,'2018-09-06 02:27:13.998640','7','Mayerlin Panay','[{"changed": {"fields": ["carnet"]}}]',16,1,2);
INSERT INTO "django_admin_log" VALUES(18,'2018-09-06 02:27:27.066611','8','Miguel Caro','[{"changed": {"fields": ["nombre", "carnet"]}}]',16,1,2);
INSERT INTO "django_admin_log" VALUES(19,'2018-09-06 02:29:18.251986','1','title','',13,1,3);
INSERT INTO "django_admin_log" VALUES(20,'2018-09-06 02:29:18.329656','3','Operativos de Salud ','',13,1,3);
INSERT INTO "django_admin_log" VALUES(21,'2018-09-06 02:29:37.473154','2','Quienes Somos','[{"changed": {"fields": ["titulo", "imagen", "cuerpo"]}}]',13,1,2);
INSERT INTO "django_admin_log" VALUES(22,'2018-09-06 02:32:59.001087','21','Alergias','[{"changed": {"fields": ["nombre", "imagen", "descripcion", "sintomas", "causas"]}}]',17,1,2);
CREATE TABLE IF NOT EXISTS "django_content_type" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "app_label" varchar(100) NOT NULL, "model" varchar(100) NOT NULL);
INSERT INTO "django_content_type" VALUES(1,'menu','bookmark');
INSERT INTO "django_content_type" VALUES(2,'dashboard','dashboardpreferences');
INSERT INTO "django_content_type" VALUES(3,'admin','logentry');
INSERT INTO "django_content_type" VALUES(4,'auth','user');
INSERT INTO "django_content_type" VALUES(5,'auth','permission');
INSERT INTO "django_content_type" VALUES(6,'auth','group');
INSERT INTO "django_content_type" VALUES(7,'contenttypes','contenttype');
INSERT INTO "django_content_type" VALUES(8,'sessions','session');
INSERT INTO "django_content_type" VALUES(9,'sites','site');
INSERT INTO "django_content_type" VALUES(10,'home','configuracionhome');
INSERT INTO "django_content_type" VALUES(11,'home','sethomeimage');
INSERT INTO "django_content_type" VALUES(12,'home','homeimage');
INSERT INTO "django_content_type" VALUES(13,'pagina','pagina');
INSERT INTO "django_content_type" VALUES(14,'pagina','clasificacion');
INSERT INTO "django_content_type" VALUES(15,'especialidad','especialidad');
INSERT INTO "django_content_type" VALUES(16,'especialista','especialista');
INSERT INTO "django_content_type" VALUES(17,'patologia','patologia');
INSERT INTO "django_content_type" VALUES(18,'articulo','articulo');
INSERT INTO "django_content_type" VALUES(19,'articulo','etiqueta');
INSERT INTO "django_content_type" VALUES(20,'articulo','clasificacion');
CREATE TABLE IF NOT EXISTS "articulo_clasificacion" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "tipo" varchar(20) NOT NULL UNIQUE, "slug_tipo" varchar(50) NOT NULL, "descripccion" text NOT NULL, "fecha_publicacion" datetime NOT NULL, "object_id" integer unsigned NULL, "content_type_id" integer NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "articulo_clasificacion" VALUES(1,'Operativo','operativo','','2018-09-06 02:11:56.095624',NULL,NULL);
CREATE TABLE IF NOT EXISTS "articulo_etiqueta" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "nombre" varchar(20) NOT NULL UNIQUE, "slug_nombre" varchar(50) NOT NULL, "descripccion" text NOT NULL);
INSERT INTO "articulo_etiqueta" VALUES(1,'La Pincoya','la-pincoya','');
CREATE TABLE IF NOT EXISTS "articulo_articulo_etiquetas" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "articulo_id" integer NOT NULL REFERENCES "articulo_articulo" ("id") DEFERRABLE INITIALLY DEFERRED, "etiqueta_id" integer NOT NULL REFERENCES "articulo_etiqueta" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "articulo_articulo_etiquetas" VALUES(1,1,1);
CREATE TABLE IF NOT EXISTS "auth_permission" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED, "codename" varchar(100) NOT NULL, "name" varchar(255) NOT NULL);
INSERT INTO "auth_permission" VALUES(1,1,'add_bookmark','Can add bookmark');
INSERT INTO "auth_permission" VALUES(2,1,'change_bookmark','Can change bookmark');
INSERT INTO "auth_permission" VALUES(3,1,'delete_bookmark','Can delete bookmark');
INSERT INTO "auth_permission" VALUES(4,1,'view_bookmark','Can view bookmark');
INSERT INTO "auth_permission" VALUES(5,2,'add_dashboardpreferences','Can add dashboard preferences');
INSERT INTO "auth_permission" VALUES(6,2,'change_dashboardpreferences','Can change dashboard preferences');
INSERT INTO "auth_permission" VALUES(7,2,'delete_dashboardpreferences','Can delete dashboard preferences');
INSERT INTO "auth_permission" VALUES(8,2,'view_dashboardpreferences','Can view dashboard preferences');
INSERT INTO "auth_permission" VALUES(9,3,'add_logentry','Can add log entry');
INSERT INTO "auth_permission" VALUES(10,3,'change_logentry','Can change log entry');
INSERT INTO "auth_permission" VALUES(11,3,'delete_logentry','Can delete log entry');
INSERT INTO "auth_permission" VALUES(12,3,'view_logentry','Can view log entry');
INSERT INTO "auth_permission" VALUES(13,4,'add_user','Can add user');
INSERT INTO "auth_permission" VALUES(14,4,'change_user','Can change user');
INSERT INTO "auth_permission" VALUES(15,4,'delete_user','Can delete user');
INSERT INTO "auth_permission" VALUES(16,4,'view_user','Can view user');
INSERT INTO "auth_permission" VALUES(17,5,'add_permission','Can add permission');
INSERT INTO "auth_permission" VALUES(18,5,'change_permission','Can change permission');
INSERT INTO "auth_permission" VALUES(19,5,'delete_permission','Can delete permission');
INSERT INTO "auth_permission" VALUES(20,5,'view_permission','Can view permission');
INSERT INTO "auth_permission" VALUES(21,6,'add_group','Can add group');
INSERT INTO "auth_permission" VALUES(22,6,'change_group','Can change group');
INSERT INTO "auth_permission" VALUES(23,6,'delete_group','Can delete group');
INSERT INTO "auth_permission" VALUES(24,6,'view_group','Can view group');
INSERT INTO "auth_permission" VALUES(25,7,'add_contenttype','Can add content type');
INSERT INTO "auth_permission" VALUES(26,7,'change_contenttype','Can change content type');
INSERT INTO "auth_permission" VALUES(27,7,'delete_contenttype','Can delete content type');
INSERT INTO "auth_permission" VALUES(28,7,'view_contenttype','Can view content type');
INSERT INTO "auth_permission" VALUES(29,8,'add_session','Can add session');
INSERT INTO "auth_permission" VALUES(30,8,'change_session','Can change session');
INSERT INTO "auth_permission" VALUES(31,8,'delete_session','Can delete session');
INSERT INTO "auth_permission" VALUES(32,8,'view_session','Can view session');
INSERT INTO "auth_permission" VALUES(33,9,'add_site','Can add site');
INSERT INTO "auth_permission" VALUES(34,9,'change_site','Can change site');
INSERT INTO "auth_permission" VALUES(35,9,'delete_site','Can delete site');
INSERT INTO "auth_permission" VALUES(36,9,'view_site','Can view site');
INSERT INTO "auth_permission" VALUES(37,10,'add_configuracionhome','Can add configuracion');
INSERT INTO "auth_permission" VALUES(38,10,'change_configuracionhome','Can change configuracion');
INSERT INTO "auth_permission" VALUES(39,10,'delete_configuracionhome','Can delete configuracion');
INSERT INTO "auth_permission" VALUES(40,10,'view_configuracionhome','Can view configuracion');
INSERT INTO "auth_permission" VALUES(41,11,'add_sethomeimage','Can add set-imagenes');
INSERT INTO "auth_permission" VALUES(42,11,'change_sethomeimage','Can change set-imagenes');
INSERT INTO "auth_permission" VALUES(43,11,'delete_sethomeimage','Can delete set-imagenes');
INSERT INTO "auth_permission" VALUES(44,11,'view_sethomeimage','Can view set-imagenes');
INSERT INTO "auth_permission" VALUES(45,12,'add_homeimage','Can add imagen');
INSERT INTO "auth_permission" VALUES(46,12,'change_homeimage','Can change imagen');
INSERT INTO "auth_permission" VALUES(47,12,'delete_homeimage','Can delete imagen');
INSERT INTO "auth_permission" VALUES(48,12,'view_homeimage','Can view imagen');
INSERT INTO "auth_permission" VALUES(49,13,'add_pagina','Can add Página');
INSERT INTO "auth_permission" VALUES(50,13,'change_pagina','Can change Página');
INSERT INTO "auth_permission" VALUES(51,13,'delete_pagina','Can delete Página');
INSERT INTO "auth_permission" VALUES(52,13,'view_pagina','Can view Página');
INSERT INTO "auth_permission" VALUES(53,14,'add_clasificacion','Can add Clasificación');
INSERT INTO "auth_permission" VALUES(54,14,'change_clasificacion','Can change Clasificación');
INSERT INTO "auth_permission" VALUES(55,14,'delete_clasificacion','Can delete Clasificación');
INSERT INTO "auth_permission" VALUES(56,14,'view_clasificacion','Can view Clasificación');
INSERT INTO "auth_permission" VALUES(57,15,'add_especialidad','Can add Especialidad');
INSERT INTO "auth_permission" VALUES(58,15,'change_especialidad','Can change Especialidad');
INSERT INTO "auth_permission" VALUES(59,15,'delete_especialidad','Can delete Especialidad');
INSERT INTO "auth_permission" VALUES(60,15,'view_especialidad','Can view Especialidad');
INSERT INTO "auth_permission" VALUES(61,16,'add_especialista','Can add Especialista');
INSERT INTO "auth_permission" VALUES(62,16,'change_especialista','Can change Especialista');
INSERT INTO "auth_permission" VALUES(63,16,'delete_especialista','Can delete Especialista');
INSERT INTO "auth_permission" VALUES(64,16,'view_especialista','Can view Especialista');
INSERT INTO "auth_permission" VALUES(65,17,'add_patologia','Can add Patología');
INSERT INTO "auth_permission" VALUES(66,17,'change_patologia','Can change Patología');
INSERT INTO "auth_permission" VALUES(67,17,'delete_patologia','Can delete Patología');
INSERT INTO "auth_permission" VALUES(68,17,'view_patologia','Can view Patología');
INSERT INTO "auth_permission" VALUES(69,18,'add_articulo','Can add Artículo');
INSERT INTO "auth_permission" VALUES(70,18,'change_articulo','Can change Artículo');
INSERT INTO "auth_permission" VALUES(71,18,'delete_articulo','Can delete Artículo');
INSERT INTO "auth_permission" VALUES(72,18,'view_articulo','Can view Artículo');
INSERT INTO "auth_permission" VALUES(73,19,'add_etiqueta','Can add Etiqueta');
INSERT INTO "auth_permission" VALUES(74,19,'change_etiqueta','Can change Etiqueta');
INSERT INTO "auth_permission" VALUES(75,19,'delete_etiqueta','Can delete Etiqueta');
INSERT INTO "auth_permission" VALUES(76,19,'view_etiqueta','Can view Etiqueta');
INSERT INTO "auth_permission" VALUES(77,20,'add_clasificacion','Can add Clasificación');
INSERT INTO "auth_permission" VALUES(78,20,'change_clasificacion','Can change Clasificación');
INSERT INTO "auth_permission" VALUES(79,20,'delete_clasificacion','Can delete Clasificación');
INSERT INTO "auth_permission" VALUES(80,20,'view_clasificacion','Can view Clasificación');
CREATE TABLE IF NOT EXISTS "auth_user" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "password" varchar(128) NOT NULL, "last_login" datetime NULL, "is_superuser" bool NOT NULL, "username" varchar(150) NOT NULL UNIQUE, "first_name" varchar(30) NOT NULL, "email" varchar(254) NOT NULL, "is_staff" bool NOT NULL, "is_active" bool NOT NULL, "date_joined" datetime NOT NULL, "last_name" varchar(150) NOT NULL);
INSERT INTO "auth_user" VALUES(1,'argon2$argon2i$v=19$m=512,t=2,p=2$MGlOcU1wYkFDbE0w$UTe5lORDmr/+mIhX5Kw1Cw','2018-09-06 02:11:31.006245',1,'admin','','dpineda@uchile.cl',1,1,'2018-09-06 02:11:20.449200','');
CREATE TABLE IF NOT EXISTS "admin_tools_dashboard_preferences" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "data" text NOT NULL, "dashboard_id" varchar(100) NOT NULL, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "admin_tools_dashboard_preferences" VALUES(1,'{}','dashboard',1);
INSERT INTO "admin_tools_dashboard_preferences" VALUES(2,'{}','home-dashboard',1);
INSERT INTO "admin_tools_dashboard_preferences" VALUES(3,'{}','pagina-dashboard',1);
CREATE TABLE IF NOT EXISTS "especialidad_especialidad" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "titulo" varchar(100) NOT NULL, "slug_titulo" varchar(50) NOT NULL UNIQUE, "imagen" varchar(100) NOT NULL, "fecha_publicacion" datetime NOT NULL, "resumen" text NOT NULL, "cuerpo" text NOT NULL);
INSERT INTO "especialidad_especialidad" VALUES(2,'La Naturopatía ','la-naturopatia','','2018-09-06 02:23:42.449063','Escribe un resumen','La Naturopatía o también conocida como Medicina Natural, incluye un conjunto de disciplinas, técnicas o procedimientos preventivos, diagnósticos, terapéuticos y rehabilitadores, que han sido validados científicamente en diversas latitudes, por la tradición y por investigaciones. En algunos países ya se encuentra integrado a los servicios y redes de atención de la salud pública.

Reconocida la sabiduría y los importantes efectos de estas prácticas ancestrales, aplicamos los productos y los procedimientos de la Digitopuntura, el Ajuste manual y la Apiterapia para tratar diversas patologías. La combinación de estas tres técnicas durante la sesión terapéutica es la que nos brinda la seguridad y efectividad necesarias para desempeñarnos como Naturópatas
');
INSERT INTO "especialidad_especialidad" VALUES(3,'Digitopuntura','digitopuntura','','2018-09-06 02:23:42.526965','Escribe un resumen','Es una técnica de curación que alivia o reduce los dolores y otros síntomas de diversas enfermedades. La Digitopuntura proviene de la antigua China y está muy relacionada con otra disciplina de medicina natural conocida como acupuntura. Utilizando sus mismos puntos, con los dedos se realiza una presión que estimula y desbloquea energías, mejora las funciones del sistema circulatorio, relaja el sistema nervioso, devolviendo el equilibrio y eliminando diversas patologías.

En nuestro caso también es empleada para diagnosticar patologías básicas, dolencias o malestares que pueda presentar el paciente, a través de diversos puntos y la presión ejercida en ellos es posible recoger los antecedentes necesarios para efectuar el tratamiento complementándolo con la apiterapia y otras disciplinas.
');
INSERT INTO "especialidad_especialidad" VALUES(4,'La Apiterapia ','la-apiterapia','','2018-09-06 02:23:42.604677','Escribe un resumen','Esta disciplina terapéutica tiene innumerables efectos en el cuerpo, relacionados con el desgaste, la inflamación, el dolor, contracturas, el desequilibrio, la pérdida de funcionalidad, la autoinmunidad, retrasa el envejecimiento y mejora la interconexión entre los órganos del cuerpo entre otras variadas funciones terapéuticas.

La Apiterapia es una terapia milenaria, que usa el veneno de las abejas (Apitoxina) para el tratamiento de diversas enfermedades, teniendo en cuenta la teoría de meridianos y puntos de acupuntura (en nuestro caso el uso de la Digitopuntura) para su aplicación. También utiliza los productos de las abejas tales como, miel de abejas, polén, propoleo, jalea real, entre otros, con el fin de prevenir y acompañar el tratamiento de distintos padecimientos. No en vano, Hipócrates la describió como la &quot;Farmacia del Cielo&quot;.

El veneno de abeja es un líquido claro y de reacción ácida, llamado Apitoxina, que contiene 88% de agua, además de enzimas que son proteínas, péptidos, aminoácidos y compuestos volátiles que se encuentran en gran cantidad biológica en los seres vivos. &quot;Lo que permite su efecto curativo&quot;.

Los principales componentes de la Apitoxina son: La Apamina, es un neurotransmisor que aumenta el nivel de cortisona en la sangre, contribuyendo a los efectos antiinflamatorios de la apitoxina; La Melitina: antibiótico natural 100 veces más potente que la penicilina en su actividad antibacteriana; y el Péptido 401, que posee efecto antiinflamatorio 100 veces más efectivo que la hidrocortisona (reduce el dolor y la inflamación).

Fuente: Dr. Vicente Ferrer, bioquímico que se dedicó al estudio de la apiterapia en Chile.
');
INSERT INTO "especialidad_especialidad" VALUES(5,'Tratamiento manual de subluxaciones en columna y articulaciones.','tratamiento-manual-de-subluxaciones-en-columna-y-a','','2018-09-06 02:23:42.693694','Escribe un resumen','Esta es una técnica para trabajar sobre problemas y desajustes de la columna vertebral y las articulaciones periféricas, efectuando correcciones con nuestras manos al aparato musculo-esquelético para su buen funcionamiento. Este es un método no invasivo, eficiente, de excelentes y rápidos resultados.

Sus orígenes se remontan a Hipócrates, (350 años A.C.) que destacaba la importancia de la columna y articulaciones en la salud y su necesario ajuste para mantenerlas en buen funcionamiento y mejorar la calidad de vida de las personas.

La columna soporta el peso y equilibra nuestro cuerpo, protege la médula espinal y conecta el cerebro con el resto del organismo, a través del sistema nervioso central. Cualquier problema en la columna o en las articulaciones periféricas (piernas, brazos) como subluxaciones, pinzamiento de nervios, inflamación de nervios y tendones, hernias discales, dolores de cabeza y migrañas, dolores de cuello, brazos, espalda, cintura, piernas, ciática, bursitis, tendinitis, etc. sea por accidente, desgaste, edad, o movimientos repetitivos, sedentarismo y fuerzas mal hechas, afectan los músculos, ligamentos, tendones y diversos órganos o inversamente desajustes en el sistema muscular o glandular, afectan el sistema óseo y articular, la movilidad, nuestra salud física y psicológica en general.
');
CREATE TABLE IF NOT EXISTS "especialista_especialista" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "nombre" varchar(50) NOT NULL, "cargo" varchar(5) NOT NULL, "email" varchar(254) NOT NULL, "carnet" varchar(100) NOT NULL, "rut" varchar(20) NOT NULL, "telefono" varchar(128) NOT NULL, "orden" integer NOT NULL, "linkedin" varchar(200) NOT NULL, "slug_nombre" varchar(50) NOT NULL, "twitter" varchar(200) NOT NULL);
INSERT INTO "especialista_especialista" VALUES(4,'Juan Panay','PR','jpanay@naturopatiachile.cl','especialistas/juan-panay/naturopatiachile-naturopata-juan_panay_bymKDB5.jpg','7.691.137-1','+56982710863',0,'','juan-panay','');
INSERT INTO "especialista_especialista" VALUES(5,'Daniel Fuentes','ASM','dfuentes@naturopatiachile.cl','especialistas/daniel-fuentes/naturopatiachile-naturopata-daniel_fuentes_GuXLzZl.jpg','17.266.367-2','+56961597087',0,'','daniel-fuentes','');
INSERT INTO "especialista_especialista" VALUES(6,'Charles Varga','ASM','cvargas@naturopatiachile.cl','especialistas/charles-varga/naturopatiachile-naturopata-charles_vargas_sEl30W4.jpg','16.082.326-7','+56967879167',0,'','charles-varga','');
INSERT INTO "especialista_especialista" VALUES(7,'Mayerlin Panay','ASM','mpanay@naturopatiachile.cl','especialistas/mayerlin-panay/naturopatiachile-naturopata-mayerlin_panay_jJZQ7j2.jpg','14.192.667-5','+56961621722',0,'','mayerlin-panay','');
INSERT INTO "especialista_especialista" VALUES(8,'Miguel Caro','ASM','mcaro@naturopatiachile.cl','especialistas/miguel-caro/naturopatiachile-naturopata-miguel_caro_2JBiAjF.jpg','16.903.387-0','+56991580423',0,'','miguel-caro','');
INSERT INTO "especialista_especialista" VALUES(9,'Gladys Muñoz','ASM','gmunoz@naturopatiachile.cl','especialistas/gladys-munoz/naturopatiachile-naturopata-gladys-munoz-gonzalez_FUiPLQ1.jpg','11.189.791-3','+56985142374',0,'','gladys-munoz','');
INSERT INTO "especialista_especialista" VALUES(10,'María Fuentes','ASM','mfuentes@naturopatiachile.cl','especialistas/maria-fuentes/naturopatiachile-naturopata-maria-fuentes-aros_dGwaLta.jpg','10.528.471-3','+56982046650',0,'','maria-fuentes','');
CREATE TABLE IF NOT EXISTS "home_sethomeimage" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "nombre" varchar(200) NOT NULL, "fecha" date NOT NULL);
INSERT INTO "home_sethomeimage" VALUES(1,'Base','2018-09-06');
CREATE TABLE IF NOT EXISTS "home_homeimage" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "imagen" varchar(100) NOT NULL, "etiqueta" varchar(100) NOT NULL, "set_images_id" integer NULL REFERENCES "home_sethomeimage" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "home_homeimage" VALUES(1,'home/imagen/portada_corte_Y1tR7zH.jpg','grupo',1);
CREATE TABLE IF NOT EXISTS "home_configuracionhome" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "nombre" varchar(200) NOT NULL, "slug_nombre" varchar(50) NOT NULL, "mostrar_especialista" bool NOT NULL, "activar_blog" bool NOT NULL, "activar_conf" bool NOT NULL, "set_images_id" integer NULL REFERENCES "home_sethomeimage" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "home_configuracionhome" VALUES(1,'Base','base',1,0,1,1);
CREATE TABLE IF NOT EXISTS "admin_tools_menu_bookmark" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "url" varchar(255) NOT NULL, "title" varchar(255) NOT NULL, "user_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE TABLE IF NOT EXISTS "pagina_clasificacion" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "tipo" varchar(20) NOT NULL UNIQUE, "slug_tipo" varchar(50) NOT NULL, "descripccion" text NOT NULL, "fecha_publicacion" datetime NOT NULL, "object_id" integer unsigned NULL, "content_type_id" integer NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "pagina_clasificacion" VALUES(1,'Informativa','informativa','','2018-09-06 02:23:49.989723',NULL,NULL);
CREATE TABLE IF NOT EXISTS "pagina_pagina" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "titulo" varchar(100) NOT NULL, "slug_titulo" varchar(50) NOT NULL UNIQUE, "imagen" varchar(100) NOT NULL, "fecha_publicacion" datetime NOT NULL, "resumen" text NOT NULL, "cuerpo" text NOT NULL, "object_id" integer unsigned NULL, "clasificacion_id" integer NULL REFERENCES "pagina_clasificacion" ("id") DEFERRABLE INITIALLY DEFERRED, "content_type_id" integer NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED);
INSERT INTO "pagina_pagina" VALUES(2,'Quienes Somos','quienes-somos','paginas/quienes-somos/portada_ZXHmDV1.jpg','2018-09-06 02:29:37.472079','Escribe un resumen','<p>Somos una Asociaci&oacute;n de Apiterapeutas y Naturopatas fundada en el a&ntilde;o 2008, compuesta por diferentes terapeutas a lo largo de Chile. Nos organizamos como Asociaci&oacute;n luego de realizar un diagn&oacute;stico cr&iacute;tico de la salud y de las condiciones de vida de la sociedad en nuestro pa&iacute;s. Nuestro objetivo como Asociaci&oacute;n es aportar de una forma concreta al bienestar y la calidad de vida de los individuos, las familias y las comunidades de los diferentes sectores de la sociedad, a trav&eacute;s de pr&aacute;cticas naturales y alternativas de car&aacute;cter integral, de calidad y efectivas, para la recuperaci&oacute;n y/o conservaci&oacute;n de la salud. Nos formamos como Naturopatas teniendo como principal l&iacute;nea de trabajo, la socializaci&oacute;n de experiencia y la investigaci&oacute;n cient&iacute;fica, promoviendo la articulaci&oacute;n de redes en el campo de la salud, contribuyendo al intercambio de experiencias y saberes de la medicina complementaria y tradicional de los pueblos milenarios, y principalmente, construir alianzas internacionales que impulsen la cooperaci&oacute;n entre diferentes instituciones ligadas al campo de la salud.</p>',NULL,1,NULL);
CREATE TABLE IF NOT EXISTS "patologia_patologia" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "nombre" varchar(50) NOT NULL, "imagen" varchar(100) NOT NULL, "slug_nombre" varchar(50) NOT NULL, "descripcion" text NOT NULL, "sintomas" text NOT NULL, "causas" text NOT NULL, "fecha_publicacion" datetime NOT NULL);
INSERT INTO "patologia_patologia" VALUES(2,'Síndrome Del Túnel Carpiano','patologias/sindrome-del-tunel-carpiano/naturopatiachile-apiterapia-tunel-carpiano2.jpg','sindrome-del-tunel-carpiano','El síndrome del túnel carpiano se reconoce por un fuerte dolor en la muñeca, lugar donde se encuentra el túnel carpiano, este túnel es una zona estrecha por donde pasa el nervio medio hacia la mano, por lo tanto, cualquier inflamación comprime este nervio causando dolor, entumecimiento y /o debilidad en la mano.

&amp;nbsp;
','
	Torpeza de la mano al agarrar objetos
	Entumecimiento en los dedos
	Dolor en la mano o muñeca que a veces se extiende al codo
	Debilidad en las manos
	Problemas de coordinación de los dedos

','
	Movimiento repetitivos de mano y muñeca
	Uso de herramientas manuales pesadas o que vibren
	Fractura de huesos
	Quiste o tumor en la muñeca
	Obesidad
	Artritis reumatoide

','2018-09-06 02:23:57.892914');
INSERT INTO "patologia_patologia" VALUES(3,'Tendinitis En Brazos Y Hombros','patologias/tendinitis-en-brazos-y-hombros/naturopatiachile-apiterapia-tendinitis-brazos.jpg','tendinitis-en-brazos-y-hombros','Los tendones son una estructura fibrosa que une los músculos con los huesos, la tendinitis es la inflamación e irritación de un tendón.
','
	Dolor y sensibilidad a lo largo de un tendón
	Dolor en las articulaciones
	Dolor por las noches en la zona afectada
	Dolor que empeora con el movimiento

','
	Lesión o sobrecarga
	Pérdida de elasticidad en los tendones a causa de la edad
	Artritis reumatoide
	Diabetes
	Desajuste cervical y contracturas en los brazos
	Falta de musculatura en la zona afectada

','2018-09-06 02:23:57.992868');
INSERT INTO "patologia_patologia" VALUES(4,'Tendinitis Aquileana ','patologias/tendinitis-aquileana/naturopatiachile-apiterapia-tendinitis-aquileana.jpg','tendinitis-aquileana','También conocida como tendinitis del tendón de Aquiles, este tendón es quien conecta la parte posterior de la pierna al talón permitiendo empujar el pie hacia abajo, caminar, correr y saltar. Cuando este tendón se inflama se denomina tendinitis aquileana.
','
	Dolor en el talón al caminar o correr
	Dolor o rigidez en el talón por las mañanas
	Hinchazón y calor en el talón

','
	Aumento en cantidad o intensidad en una actividad determinada
	Tensión en los gemelos
	Correr con frecuencia o sobre superficies duras
	Saltar mucho o practicar baloncesto
	Uso de calzado inadecuado
	Artritis
	Espolón calcáneo
	Pie plano

','2018-09-06 02:23:58.092838');
INSERT INTO "patologia_patologia" VALUES(5,'Osteoartritis','patologias/osteoartritis/osteoartritis.jpg','osteoartritis','Es el envejecimiento, desgaste y ruptura de una acticulación, afectando principalmente al cartílago. Como consecuencia los huesos empiezan a rozarse provocando mucho dolor
','
	Dolor y rigidez articular
	Hinchazón o sensibilidad al tocar las articulaciones
	Crujidos en las articulaciones

','
	Sobrepeso
	Envejecimiento
	Lesiones de las articulaciones
	Deformación o defecto genético en el cartílago
	Fatiga articular
	Trabajos forzados
	Práctica de deportes de alto impacto

','2018-09-06 02:23:58.248441');
INSERT INTO "patologia_patologia" VALUES(6,'Lupus ','patologias/lupus/lupus.jpg','lupus','Es una enfermedad autoinmune, que puede afectar a cualquier órgano y/o sistema del cuerpo. Sucede cuando el sistema inmunológico ataca las células y los tejidos sanos por error. Las mujeres presentan mayor riesgo a padecer lupus.
','
	Dolor o inflamación en las articulaciones
	Dolores musculares
	Fiebre de origen desconocido
	Cansancio
	Erupciones cutáneas rojas en la cara
	Pérdida de peso
	Caída del cabello
	Danos en los riñones, provocando hinchazón en cara y piernas
	Trastornos psiquiátricos
	Cefaleas
	Depresión

','
	Alteración en el sistema inmunológico
	Genética
	Factores ambientales
	Trastornos en las hormonas femeninas


&amp;nbsp;
','2018-09-06 02:23:58.337367');
INSERT INTO "patologia_patologia" VALUES(7,'Lumbalgia ','patologias/lumbalgia/lumbalgia.jpg','lumbalgia','También conocido como lumbago, se refiere a un dolor agudo que se siente en la zona lumbar o espalda baja. También se puede presentar rigidez en la zona, disminución del movimiento y dificultas para pararse derecho o ponerse de pie.
','
	Hormigueo o ardor en la zona lumbar
	Dolor en la zona lumbar
	Dolor o sensibilidad en las piernas, cadera o pies

','
	Sedentarismo y sobrepeso
	Movimientos bruscos o realizar cargas pesadas
	Cáncer que compromete la columna
	Osteoporosis
	Fractura de la médula espinal
	Tensión muscular
	Hernia discal
	Ciática
	Escoliosis

','2018-09-06 02:23:58.426283');
INSERT INTO "patologia_patologia" VALUES(8,'Fibromialgia','patologias/fibromialgia/fibromialgia.jpg','fibromialgia','La fibromialgia es un trastorno en el sistema inmunológico, causando dolores musculares, fatiga (cansancio) y dolor y sensibilidad en todo el cuerpo
','
	Dificultad para dormir
	Rigidez por las mañanas
	Dolores de cabeza
	Periodos menstruales dolorosos
	Sensación de hormigueo y adormecimiento en manos y pies
	Falta de memoria o dificultad para concentrarse
	Ansiedad
	Depresión
	Dolor muscular

','
	Alteración de determinados neurotransmisores del sistema nervioso
	Trastorno del sistema inmunológico
	Acontecimientos estresantes o traumáticos
	Lesiones recurrentes
	Malestares o dolencias físicas
	Artritis reumatoide
	Artritis espinal
	Lupus

','2018-09-06 02:23:58.515039');
INSERT INTO "patologia_patologia" VALUES(9,'Fascitis Plantar','patologias/fascitis-plantar/fascitis-plantar.jpg','fascitis-plantar','Es la inflamación de la fascia plantar, esta es el tejido grueso de la planta del pie que conecta el calcáneo a los dedos creando el arco del pie
','
	Dolor y rigidez en la parte inferior del talón
	Dolor en la planta del pie

','
	Sobrecarga en los pies
	Pie plano o arco plantar alto
	Correr largas distancias
	Obesidad o aumento repentino de peso
	Tendinitis aquileana
	Uso de zapatos con suela blanda

','2018-09-06 02:23:58.622434');
INSERT INTO "patologia_patologia" VALUES(10,'Espolón Calcáneo','patologias/espolon-calcaneo/espolon-calcaneo.jpg','espolon-calcaneo','Es una calcificación osea que se forma en la parte inferior del hueso del talón llamado calcáneo, esta formación es en muchos casos consecuencia de una fascitis plantar
','
	Dolor en el talón
	Sensación de pisar un clavo
	Estos síntomas se sienten generalmente en las mañanas

','
	Exceso de ácido úrico en el cuerpo
	Práctica deportiva intensa
	Caminar o estar de pie por periodos largos
	Edad elevada
	Obesidad o sobrepeso
	Pie plano
	Uso de zapatos inadecuados

','2018-09-06 02:23:58.704706');
INSERT INTO "patologia_patologia" VALUES(11,'Esclerosis Múltiple ','patologias/esclerosis-multiple/esclerosis-multiple.jpg','esclerosis-multiple','Es una enfermedad autoinmune que afecta el cerebro y la medula espinal, es causada por el daño a la vaina de mielina, esta vaina es la cubierta que protege las neuronas, cuando esta cubierta se daña los impulsos nerviosos disminuyen o se detienen. Esta enfermedad afecta principalmente a las mujeres y se frecuenta entre los veinte y cuarenta años de edad
','
	Pérdida de equilibrio
	Espasmos musculares
	Problemas para mover brazos y piernas
	Debilidad, temblor, picazón u hormigueo en brazos y piernas
	Estreñimiento y escape de heces
	Dificultad para comenzar a orinar
	Incontinencia urinaria
	Visión doble, molestia en los ojos o pérdida de visión
	Dolor facial
	Pérdida de memoria, problemas de concentración
	Depresión o sentimientos de tristeza
	Problemas de erección o lubricación vaginal

','
	Ataque al sistema nervioso por células inmunitarias
	Virus o defecto genético
	Factores ambientales
	Trastornos del sistema inmunológico

','2018-09-06 02:23:58.792915');
INSERT INTO "patologia_patologia" VALUES(12,'Esclerodermia','patologias/esclerodermia/esclerodermia.jpg','esclerodermia','Es una enfermedad autoinmune donde el sistema inmunológico ataca por error destruyendo el tejido corporal sano, esto involucra cambios en la piel, los vasos sanguíneos, músculos y órganos internos. Afecta generalmente a personas entre treinta y cincuenta años de edad
','
	Dedos de las manos o pies de color azulados o blancos en respuestas a temperaturas frías
	Pérdida de cabello
	Piel mas clara o más oscura de lo normal
	Rigidez y tensión en los dedos de manos o pies
	Piel facial tensa y con aspecto de máscara
	Dolor, rigidez y/o inflamación en dedos, pies y articulaciones
	Tos seca y dificultad respiratoria
	Estreñimiento o diarrea
	Dificultad para tragar
	Reflujo estomacal

','
	Acumulación de colágeno en la piel y otros órganos del cuerpo
	Padecer enfermedades como lupus o polimiositis

','2018-09-06 02:23:58.881829');
INSERT INTO "patologia_patologia" VALUES(13,'Dolor de Hombro ','','dolor-de-hombro','El dolor de hombro es cualquier dolor que involucre la articulación de éste. El hombro es la articulación de mayor movilidad en el cuerpo humano
','
	Dolor en el hombro con fiebre, hinchazón o enrojecimiento
	Problemas para mover el hombro
	Dolor que dura mas de una o dos semanas
	Hinchazón en el hombro
	Coloración roja o morada sobre la piel cercana al hombro

','
	Tendinitis del manguito rotador
	Artritis en la articulación
	Espolones óseos en el área del hombro
	Bursitis
	Fracturas de los huesos del hombro
	Dislocación o separación del hombro
	Síndrome del hombro congelado
	Sobrecarga o lesión de los tendones cercanos
	Desgarro de los tendones

','2018-09-06 02:23:58.981724');
INSERT INTO "patologia_patologia" VALUES(14,'Dolor de Espalda ','patologias/dolor-de-espalda/dolor-de-espalda.jpg','dolor-de-espalda','Es uno de los problemas médicos más comunes en las personas, este dolor puede varias desde un dolor sordo, constante, hasta un dolor súbito e intenso
','
	Dolor, rigidez y/o inflamación en zonas de la espalda
	Tensión muscular

','
	Edad
	Degeneración de los discos intervertebrales
	Trabajos forzados
	Estilo de vida sedentario
	Sobrepeso
	Mala postura
	Embarazo
	Fumar
	Hernia de disco lumbar
	Estenosis espinal lumbar
	Espondilolistesis
	Osteoartritis

','2018-09-06 02:23:59.092416');
INSERT INTO "patologia_patologia" VALUES(15,'Ciática ','patologias/ciatica/ciatica.jpg','ciatica','Se refiere a dolor, debilidad u hormigueo en la pierna y es causada por lesión o presión sobre el nervio ciático. Este nervio comienza en la región lumbar y baja por la parte posterior de cada pierna.
','
	Dolor, debilidad, entumecimiento o ardor en zona lumbar, piernas, cadera o pies.
	Estos síntomas sueles presentarse después de pararse o sentarse, por las noches, al doblarse hacia atrás o caminar periodos prolongados

','
	Hernia de disco
	Estenosis raquídea
	Síndrome piriforme
	Lesión o fractura de la pelvis
	Tumores
	Embarazo
	Práctica de deportes de alto impacto
	Golpe o lesión que involucre caderas o piernas

','2018-09-06 02:23:59.171427');
INSERT INTO "patologia_patologia" VALUES(16,'Cefaleas ','patologias/cefaleas/cefaleas.jpg','cefaleas','La cefalea es un dolor de cabeza que se presenta de forma intermitente, existen distintos tipos de cefaleas; de los senos paranasales, en brotes, por tensión y migraña. La cefalea por tensión es la más común de todas
','','
	Edad avanzada
	Herencia
	Estrés, tensión, depresión y/o ansiedad
	Alimentos que contengan tiramina o glutamato monosódico
	Consumo de alcohol y/o cigarro
	Cambios hormonales
	Cambios climáticos
	Falta o exceso de sueño
	Abuso de fármacos o cafeína
	Obesidad o sobrepeso

','2018-09-06 02:23:59.270683');
INSERT INTO "patologia_patologia" VALUES(17,'Bursitis ','patologias/bursitis/bursitis.jpg','bursitis','Es la inflamación e irritación de una bursa, ésta es una bolsa llena de líquido que actúa como amortiguador entre los músculos, tendones y huesos. La bursitis se presenta en el hombro, rodilla, codo y cadera.
','
	Sensibilidad y dolor articular
	Rigidez cuando se mueve la articulación afectada
	Hinchazón, calor o enrojecimiento sobre la zona afectada

','
	Sobrecarga de actividades físicas
	Sobrepeso
	Artritis reumatoide
	Traumatismo
	Gota
	Infecciones

','2018-09-06 02:23:59.370671');
INSERT INTO "patologia_patologia" VALUES(18,'Asma Bronquial ','patologias/asma-bronquial/asma-bronquial.jpg','asma-bronquial','Es una enfermedad pulmonar crónica, producida por una inflamación bronquial que obstruye la vía aérea de manera total o parcial
','
	Ahogo
	Tos
	Pecho apretado
	Falta de aire
	Dificultad para respirar
	Respiración sibilante
	Pulso rápido
	Ansiedad

','
	Caspa o pelaje de animales
	Genéticas
	Ejercicio
	Obesidad
	Ácaros del polvo
	Moho
	Polen
	Ciertos medicamentos
	Infecciones respiratorias
	Emociones fuertes
	Humo del tabaco

','2018-09-06 02:23:59.470598');
INSERT INTO "patologia_patologia" VALUES(19,'Artrosis ','patologias/artrosis/artrosis.jpg','artrosis','Es una enfermedad degenerativa causada por el deterioro del cartílago articular, puede llegar un momento en el que los cartílagos desaparecen y aparece el dolor. Cuando esto sucede el hueso reacciona y crece por los lados produciendo la deformación de la articulación. La artrosis se presenta en rodillas, manos, cadera y columna
','
	Dolor articular
	Limitación de los movimientos
	Crujidos en las articulaciones
	Derrame articular
	Rigidez y deformidad articular

','
	Sobrecarga de presión sobre un cartílago
	Fisura en un cartílago
	Presencia de proteínas como el colágeno y proteoglicanos

','2018-09-06 02:23:59.570695');
INSERT INTO "patologia_patologia" VALUES(20,'Artrosis Reumatoide ','patologias/artrosis-reumatoide/artrosis-reumatoide.jpg','artrosis-reumatoide','Es una enfermedad autoinmune que consiste en la inflamación de las articulaciones y tejidos circundantes. Esta enfermedad a largo plazo puede ser crónica
','
	Dolor articular
	Rigidez, calor, sensibilidad, pérdida de movimiento y/o deformación articular
	Fatiga
	Dolor torácico al respirar
	Resequedad en ojos y boca
	Ardor y secreción en los ojos
	Dificultades para dormir
	Entumecimiento, hormigueo o ardor en manos y pies

','
	Debilidad y trastornos del sistema inmunológico
	Infecciones
	Genes
	Desorden hormonal

','2018-09-06 02:23:59.718467');
INSERT INTO "patologia_patologia" VALUES(21,'Alergias','patologias/alergias/naturopatiachile-apiterapia-alergias.jpg','alergias','<p>Son respuestas exageradas del sistema inmunol&oacute;gico al entrar en contacto con determinadas sustancias llamadas al&eacute;rgenos, que por lo general no afectan a la mayor&iacute;a de las personas</p>','<p>Goteos nasales y/o estornudos Picaz&oacute;n Sarpullidos Hinchaz&oacute;n Asma</p>','<p>Sistema inmunol&oacute;gico debilitado Polen &Aacute;caros del polvo Esporas de moho Caspa de animales Algunas alimentos Picaduras de insectos Determinados medicamentos</p>','2018-09-06 02:32:58.999704');
CREATE TABLE IF NOT EXISTS "django_session" ("session_key" varchar(40) NOT NULL PRIMARY KEY, "session_data" text NOT NULL, "expire_date" datetime NOT NULL);
INSERT INTO "django_session" VALUES('6bjnogu0zcrxslr1s5kpqcvmh0zrwjfp','NjFjNDczOTc1NDU2Nzc2ZTRkNTljN2UxNDliYzYyYTE0MzkxNTFjNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMDZkNTNmYzU5NTk2MTY3MjAxOGE5ODUwODFmODA3Y2JhZTBhYTAwMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-09-20 02:11:31.080365');
CREATE TABLE IF NOT EXISTS "django_site" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(50) NOT NULL, "domain" varchar(100) NOT NULL UNIQUE);
INSERT INTO "django_site" VALUES(1,'example.com','example.com');
CREATE TABLE IF NOT EXISTS "articulo_articulo" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "titulo" varchar(100) NOT NULL, "slug_titulo" varchar(50) NOT NULL UNIQUE, "imagen" varchar(100) NOT NULL, "fecha_publicacion" datetime NOT NULL, "resumen" text NOT NULL, "cuerpo" text NOT NULL, "object_id" integer unsigned NULL, "clasificacion_id" integer NULL REFERENCES "articulo_clasificacion" ("id") DEFERRABLE INITIALLY DEFERRED, "content_type_id" integer NULL REFERENCES "django_content_type" ("id") DEFERRABLE INITIALLY DEFERRED, "autor" varchar(60) NOT NULL);
INSERT INTO "articulo_articulo" VALUES(1,'Primer operativo en la Pincoya','primer-operativo-en-la-pincoya','articulos/primer-operativo-en-la-pincoya/operativo_de_salud_1__QWllzYw.jpg','2018-09-06 02:19:21.045645','Escribe un resumen','<p>La Actividad fue desarrollada en conjunto con el Comite de Vivienda al Pie del Cerro La Pincoya, porque comprendemos que los Derechos Humanos fundamentales, son la Salud, La Vivienda, El Trabajo, La Educaci&oacute;n y La Cultura, rescatamos la Solidaridad como valor fundamental de la pr&aacute;ctica de la Naturopat&iacute;a, adem&aacute;s de aportar a las necesidades reales y concretas de la Poblaci&oacute;n. Creemos en la organizaci&oacute;n social como base fundamental de las relaciones &nbsp;comunitarias.</p>',NULL,1,NULL,'Belinda Buzzone');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('django_migrations',27);
INSERT INTO "sqlite_sequence" VALUES('django_admin_log',22);
INSERT INTO "sqlite_sequence" VALUES('django_content_type',20);
INSERT INTO "sqlite_sequence" VALUES('auth_permission',80);
INSERT INTO "sqlite_sequence" VALUES('auth_user',1);
INSERT INTO "sqlite_sequence" VALUES('especialista_especialista',10);
INSERT INTO "sqlite_sequence" VALUES('home_homeimage',1);
INSERT INTO "sqlite_sequence" VALUES('home_configuracionhome',1);
INSERT INTO "sqlite_sequence" VALUES('django_site',1);
INSERT INTO "sqlite_sequence" VALUES('articulo_clasificacion',1);
INSERT INTO "sqlite_sequence" VALUES('articulo_etiqueta',1);
INSERT INTO "sqlite_sequence" VALUES('articulo_articulo',1);
INSERT INTO "sqlite_sequence" VALUES('articulo_articulo_etiquetas',1);
INSERT INTO "sqlite_sequence" VALUES('admin_tools_dashboard_preferences',3);
INSERT INTO "sqlite_sequence" VALUES('home_sethomeimage',1);
INSERT INTO "sqlite_sequence" VALUES('especialidad_especialidad',5);
INSERT INTO "sqlite_sequence" VALUES('pagina_clasificacion',1);
INSERT INTO "sqlite_sequence" VALUES('pagina_pagina',3);
INSERT INTO "sqlite_sequence" VALUES('patologia_patologia',21);
CREATE UNIQUE INDEX auth_group_permissions_group_id_permission_id_0cd325b0_uniq ON "auth_group_permissions" ("group_id", "permission_id");
CREATE INDEX "auth_group_permissions_group_id_b120cbf9" ON "auth_group_permissions" ("group_id");
CREATE INDEX "auth_group_permissions_permission_id_84c5c92e" ON "auth_group_permissions" ("permission_id");
CREATE UNIQUE INDEX auth_user_groups_user_id_group_id_94350c0c_uniq ON "auth_user_groups" ("user_id", "group_id");
CREATE INDEX "auth_user_groups_user_id_6a12ed8b" ON "auth_user_groups" ("user_id");
CREATE INDEX "auth_user_groups_group_id_97559544" ON "auth_user_groups" ("group_id");
CREATE UNIQUE INDEX auth_user_user_permissions_user_id_permission_id_14a6b632_uniq ON "auth_user_user_permissions" ("user_id", "permission_id");
CREATE INDEX "auth_user_user_permissions_user_id_a95ead1b" ON "auth_user_user_permissions" ("user_id");
CREATE INDEX "auth_user_user_permissions_permission_id_1fbb5f2c" ON "auth_user_user_permissions" ("permission_id");
CREATE INDEX "django_admin_log_content_type_id_c4bce8eb" ON "django_admin_log" ("content_type_id");
CREATE INDEX "django_admin_log_user_id_c564eba6" ON "django_admin_log" ("user_id");
CREATE UNIQUE INDEX django_content_type_app_label_model_76bd3d3b_uniq ON "django_content_type" ("app_label", "model");
CREATE INDEX "articulo_clasificacion_slug_tipo_8f9e32fc" ON "articulo_clasificacion" ("slug_tipo");
CREATE INDEX "articulo_clasificacion_content_type_id_0bb4c263" ON "articulo_clasificacion" ("content_type_id");
CREATE INDEX "articulo_etiqueta_slug_nombre_16f2fb68" ON "articulo_etiqueta" ("slug_nombre");
CREATE UNIQUE INDEX articulo_articulo_etiquetas_articulo_id_etiqueta_id_a9799940_uniq ON "articulo_articulo_etiquetas" ("articulo_id", "etiqueta_id");
CREATE INDEX "articulo_articulo_etiquetas_articulo_id_cf0afbbc" ON "articulo_articulo_etiquetas" ("articulo_id");
CREATE INDEX "articulo_articulo_etiquetas_etiqueta_id_3926f10d" ON "articulo_articulo_etiquetas" ("etiqueta_id");
CREATE UNIQUE INDEX auth_permission_content_type_id_codename_01ab375a_uniq ON "auth_permission" ("content_type_id", "codename");
CREATE INDEX "auth_permission_content_type_id_2f476e4b" ON "auth_permission" ("content_type_id");
CREATE UNIQUE INDEX admin_tools_dashboard_preferences_user_id_dashboard_id_74da8e56_uniq ON "admin_tools_dashboard_preferences" ("user_id", "dashboard_id");
CREATE INDEX "admin_tools_dashboard_preferences_user_id_8f768e6c" ON "admin_tools_dashboard_preferences" ("user_id");
CREATE INDEX "especialista_especialista_slug_nombre_6de706e1" ON "especialista_especialista" ("slug_nombre");
CREATE INDEX "home_homeimage_set_images_id_8a4a3523" ON "home_homeimage" ("set_images_id");
CREATE INDEX "home_configuracionhome_slug_nombre_29516f2b" ON "home_configuracionhome" ("slug_nombre");
CREATE INDEX "home_configuracionhome_set_images_id_b2186fbc" ON "home_configuracionhome" ("set_images_id");
CREATE INDEX "admin_tools_menu_bookmark_user_id_0382e410" ON "admin_tools_menu_bookmark" ("user_id");
CREATE INDEX "pagina_clasificacion_slug_tipo_b1102d5f" ON "pagina_clasificacion" ("slug_tipo");
CREATE INDEX "pagina_clasificacion_content_type_id_dab8707d" ON "pagina_clasificacion" ("content_type_id");
CREATE INDEX "pagina_pagina_clasificacion_id_8daed73f" ON "pagina_pagina" ("clasificacion_id");
CREATE INDEX "pagina_pagina_content_type_id_aefe2982" ON "pagina_pagina" ("content_type_id");
CREATE INDEX "patologia_patologia_slug_nombre_85c8d1b0" ON "patologia_patologia" ("slug_nombre");
CREATE INDEX "django_session_expire_date_a5c62663" ON "django_session" ("expire_date");
CREATE INDEX "articulo_articulo_clasificacion_id_474af348" ON "articulo_articulo" ("clasificacion_id");
CREATE INDEX "articulo_articulo_content_type_id_90e151b2" ON "articulo_articulo" ("content_type_id");
COMMIT;
